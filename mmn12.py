#!/bin/python
from sys import argv
from json import dumps

def transpose(lst, needle):
    position = lst.index(needle)
    newlist = lst if position == 0 else lst[0:position-1] + [lst[position], lst[position-1]] + lst[position+1:] 
    return {
        "price": position+1,
        "newlist": newlist ,
    }

def move_first(lst, needle):
    return {
        "price": lst.index(needle)+1,
        "newlist": [needle] + [i for i in lst if i!=needle]
    }

def frequent(lst, needle):
    return {
        "price": lst.index(needle)+1,
        "newlist": lst  # assuming the frequencies didn't change - neither did the list
    }

def runner(initial_list, requests, method, display_lists=False):
    assert set(requests).issubset(initial_list)
    if display_lists:
        print("*    {}".format(initial_list))
    current_list = initial_list
    sum_prices = 0
    for needle in requests:
        step = method(current_list, needle)
        sum_prices += step['price']
        current_list = step['newlist']
        if display_lists:
            print("{} -> {}".format(needle, current_list))
    return sum_prices

def all_runner(requests):
    initial_list = list(sorted(set(requests)))
    r1 = runner(initial_list, requests, move_first)
    r2 = runner(initial_list, requests, transpose)
    r3 = runner(initial_list, requests, frequent)
    return {
        'move_first': r1,
        'transpose': r2,
        'frequent': r3,
        'T_ratio': r1/r2,
        'F_ratio': r1/r3,
    }

if __name__ == '__main__':
    m = int(argv[1]) if len(argv)==2 else 1000
    solution = list(range(m,1,-1)) + list(range(1,m))*m
    print(dumps(all_runner(solution), indent=4));